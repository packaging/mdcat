#!/bin/bash

set -e

PREREQS=( "bsdtar" "curl" "fpm" )

error() {
  echo "$1"
  exit 1
}

check_prereq() {
  local PACKAGE="$1"
  which "$PACKAGE" >/dev/null || error "$PACKAGE is missing"
}

download_mdcat() {
  if [ ! -d mdcat ]; then
    local URL="https://github.com/lunaryorn/mdcat/releases/download/mdcat-${VERSION}/mdcat-${VERSION}-x86_64-unknown-linux-gnu.tar.gz"
    curl -s --head --fail --connect-timeout 2 "$URL" -o /dev/null || error "${URL} does not exist"
    curl -sL "${URL}" | bsdtar -xzf-
  fi
}

create_package() {
  fpm -s dir -t deb \
  -f \
  -n mdcat \
  --version "${VERSION}+${PATCHLEVEL}" \
  --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
  --url "https://crates.io/crates/mdcat" \
  --description "cat for CommonMark: Show CommonMark (a standardized Markdown dialect) documents on text terminals." \
  --prefix "/usr/bin" \
  -C "mdcat-${VERSION}-x86_64-unknown-linux-gnu" \
  mdcat
}

cleanup() {
  rm -rf mdcat
}

if [ $# -ge 1 ]; then
  VERSION="${1%%+*}"
  PATCHLEVEL="${1##*+}"
else
  VERSION="${CI_COMMIT_TAG%%+*}"
  PATCHLEVEL="${CI_COMMIT_TAG##*+}"
fi
for PREREQ in "${PREREQS[@]}"
do
  check_prereq "$PREREQ"
done
download_mdcat
create_package
cleanup
