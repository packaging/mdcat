# mdcat: [cat for markdown: Show markdown documents in TTYs](https://crates.io/crates/mdcat)

Create mdcat packages from github release archive.

Releases can be found in my [repo](https://packaging.gitlab.io/repo/)

## Requirements

* curl
* ruby + gem + fpm (```gem install fpm```)
* bsdtar

## Usage

Just run the create script with version parameter like  ```.gitlab-ci/create 3.14.4``` and it downloads the release file and will create a deb package. As fpm is able to build other packages too, you might adjust ```-t deb``` in function ```create_deb``` with another type e.g. _rpm_.
